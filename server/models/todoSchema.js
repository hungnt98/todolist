const mongoose = require("mongoose");

const todoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true,
        unique: true
    },
    watchers_count: {
        type: Number,
        required: true
    },
    language: {
        type: String,
        required: true
    },
    open_issues: {
        type: Number,
        required: true
    },
    is_private: {
        type: Boolean,
        required: true
    }
});

const todos = new mongoose.model("todos",todoSchema);


module.exports = todos;