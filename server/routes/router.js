const express = require("express");
const router = express.Router();
const todos = require("../models/todoSchema");



// router.get("/",(req,res)=>{
//     console.log("connect");
// });

// add todo

router.post("/todo/add",async(req,res)=>{
    // console.log(req.body);
    const {name,description,watchers_count,language,open_issues,is_private} = req.body;

    // if(!name || !description || !watchers_count || !language || !open_issues || !is_private){
    //     res.status(422).json("plz fill the data");
    // }
    const addTodo = new todos({
        name,description,watchers_count,language,open_issues,is_private
    });

    await addTodo.save();
    res.status(201).json(addTodo);
    res.setHeader('Content-Type', 'text/plain');

    // try {
        
    //     const preTodo = await users.findOne({description:description});
    //     console.log(preTodo);

    //     if(preTodo){
    //         res.status(422).json("this is todo is already present");
    //     }else{
    //         const addTodo = new todos({
    //             name,description,watchers_count,language,open_issues,is_private
    //         });

    //         await addTodo.save();
    //         res.status(201).json(addTodo);
    //         res.setHeader('Content-Type', 'text/plain');
    //         console.log(addTodo);
    //     }

    // } catch (error) {
    //     res.status(422).json(error);
    // }
})


// get todo list

router.get("/todo/list",async(req,res)=>{
    try {
        const todoList = await todos.find();
        res.status(201).json(todoList)
        console.log(todoList);
    } catch (error) {
        res.status(422).json(error);
    }
})

// get todo by id

router.get("/todo/:id",async(req,res)=>{
    try {
        console.log(req.params);
        const {id} = req.params;

        const todo = await todos.findById({_id:id});
        console.log(todo);
        res.status(201).json(todo)

    } catch (error) {
        res.status(422).json(error);
    }
})


// update todo by id

router.patch("/todo/:id",async(req,res)=>{
    try {
        const {id} = req.params;

        const todo = await todos.findByIdAndUpdate(id,req.body,{
            new:true
        });

        console.log(todo);
        res.status(201).json(todo);

    } catch (error) {
        res.status(422).json(error);
    }
})


// delete todo by id
router.delete("/todo/:id",async(req,res)=>{
    try {
        const {id} = req.params;

        const todo = await todos.findByIdAndDelete({_id:id})
        console.log(todo);
        res.status(201).json(todo);

    } catch (error) {
        res.status(422).json(error);
    }
})




module.exports = router;










