import React, { useContext, useEffect, useState } from 'react'
import { NavLink, useParams,useNavigate } from 'react-router-dom'
import { updatedata } from './context/ContextProvider'


const Edit = () => {

    // const [getuserdata, setUserdata] = useState([]);
    // console.log(getuserdata);

   const {updata, setUPdata} = useContext(updatedata)

    const history = useNavigate("");

    const [inpval, setINP] = useState({
        name: "",
        description: "",
        watchers_count: 0,
        language: "",
        open_issues: 0,
        is_private: false
    })

    const setdata = (e) => {
        console.log(e.target.value);
        const { name, value } = e.target;
        setINP((preval) => {
            return {
                ...preval,
                [name]: value
            }
        })
    }


    const { id } = useParams("");
    console.log(id);



    const getTodo = async () => {

        const res = await fetch(`http://localhost:8003/todo/${id}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        });

        const todo = await res.json();
        console.log(todo);

        if (res.status === 422 || !todo) {
            console.log("error ");

        } else {
            setINP(todo)
            console.log("get todo");

        }
    }

    useEffect(() => {
        getTodo();
    }, []);


    const updateTodo = async(e)=>{
        e.preventDefault();

        const {name,description,watchers_count,language,open_issues,is_private} = inpval;

        const res2 = await fetch(`http://localhost:8003/todo/${id}`,{
            method: "PATCH",
            headers: {
                "Content-Type": "application/json"
            },
            body:JSON.stringify({
                name,description,watchers_count,language,open_issues,is_private
            })
        });

        const todo = await res2.json();
        console.log(todo);

        if(res2.status === 422 || !todo){
            alert("fill the data");
        }else{
            history.push("/")
            setUPdata(todo);
        }

    }

    return (
        <div className="container">
            <NavLink to="/">home2</NavLink>
            <form className="mt-4">
                <div className="row">
                    <div class="mb-3 col-lg-6 col-md-6 col-12">
                        <label for="exampleInputEmail1" class="form-label">Name</label>
                        <input type="text" value={inpval.name} onChange={setdata} name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                    </div>
                    <div class="mb-3 col-lg-6 col-md-6 col-12">
                        <label for="exampleInputPassword1" class="form-label">Description</label>
                        <input type="text" value={inpval.description} onChange={setdata} name="description" class="form-control" id="exampleInputPassword1" />
                    </div>
                    <div class="mb-3 col-lg-6 col-md-6 col-12">
                        <label for="exampleInputPassword1" class="form-label">Watchers Count</label>
                        <input type="number" value={inpval.watchers_count} onChange={setdata} name="watchers_count" class="form-control" id="exampleInputPassword1" />
                    </div>
                    <div class="mb-3 col-lg-6 col-md-6 col-12">
                        <label for="exampleInputPassword1" class="form-label">Language</label>
                        <input type="text" value={inpval.language} onChange={setdata} name="language" class="form-control" id="exampleInputPassword1" />
                    </div>
                    <div class="mb-3 col-lg-6 col-md-6 col-12">
                        <label for="exampleInputPassword1" class="form-label">Open Issues</label>
                        <input type="number" value={inpval.open_issues} onChange={setdata} name="open_issues" class="form-control" id="exampleInputPassword1" />
                    </div>
                    <div class="mb-3 col-lg-6 col-md-6 col-12">
                        <label for="exampleInputPassword1" class="form-label">Private</label>
                        <input type="text" value={inpval.is_private} onChange={setdata} name="is_private" class="form-control" id="exampleInputPassword1" />
                    </div>

                    <button type="submit" onClick={updateTodo} class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    )
}

export default Edit;





