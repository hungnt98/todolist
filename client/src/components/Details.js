import React, { useEffect, useState } from 'react'
import CreateIcon from '@mui/icons-material/Create';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import WorkIcon from '@mui/icons-material/Work';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import { NavLink, useParams, useNavigate } from 'react-router-dom';


const Details = () => {

    const [todoList, setUserdata] = useState([]);
    console.log(todoList);

    const { id } = useParams("");
    console.log(id);

    const history = useNavigate();


    const getTodoList = async () => {

        const res = await fetch(`http://localhost:8003/todo/${id}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        });

        const todo = await res.json();
        console.log(todo);

        if (res.status === 422 || !todo) {
            console.log("error ");

        } else {
            setUserdata(todo)
            console.log("get todo");
        }
    }

    useEffect(() => {
        getTodoList();
    }, [])

    const deleteTodo = async (id) => {

        const res2 = await fetch(`http://localhost:8003/todo/${id}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        });

        const deleteTodo = await res2.json();
        console.log(deleteTodo);

        if (res2.status === 422 || !deleteTodo) {
            console.log("error");
        } else {
            console.log("todo deleted");
            history.push("/");
        }

    }

    return (
        <div className="container mt-3">
            <h1 style={{ fontWeight: 400 }}>Todo List</h1>

            <Card sx={{ maxWidth: 600 }}>
                <CardContent>
                    <div className="add_btn">
                        <NavLink to={`/edit/${todoList._id}`}>  <button className="btn btn-primary mx-2"><CreateIcon /></button></NavLink>
                        <button className="btn btn-danger" onClick={() => deleteTodo(todoList._id)}><DeleteOutlineIcon /></button>
                    </div>
                    <div className="row">
                        <div className="left_view col-lg-6 col-md-6 col-12">
                            <img src="/profile.png" style={{ width: 50 }} alt="profile" />
                            <h3 className="mt-3">Name: <span >{todoList.name}</span></h3>
                            <h3 className="mt-3">Description: <span >{todoList.description}</span></h3>
                            <p className="mt-3"><MailOutlineIcon />Watchers Count: <span>{todoList.watchers_count}</span></p>
                            <p className="mt-3"><WorkIcon />Language: <span>{todoList.language}</span></p>
                        </div>
                        <div className="right_view  col-lg-6 col-md-6 col-12">

                            <p className="mt-5"><PhoneAndroidIcon />Open issues: <span>{todoList.open_issues}</span></p>
                            <p className="mt-3"><LocationOnIcon />Private: <span>{todoList.private}</span></p>
                        </div>
                    </div>

                </CardContent>
            </Card>
        </div>
    )
}

export default Details
